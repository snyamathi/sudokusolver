'''
Created on Apr 28, 2013

@author: suneil
'''

class Cell:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.possibleValues = range(1, 10)
        self.answer = None
        
        # Add to the appropriate 3x3 subgroup
        cell_groups[self.get_group_number()].append(self)
            
    def remove_possible_value(self, value):
        if not value in self.possibleValues:
            return False
        else:
            index = self.possibleValues.index(value)
            del self.possibleValues[index]
            
            # If we only have one possible value left -> we are answered
            if self.number_of_possible_values() == 1:
                self.answer = self.possibleValues[0]
                return True
            else:
                return False
    
    def set_answer(self, value):
        self.answer = value
        self.possibleValues = [value]
        
    def number_of_possible_values(self):
        return len(self.possibleValues)
    
    def get_group_number(self):
        return self.x/3 + 3 * (self.y/3)
    
    def __repr__(self):
        if self.answer:
            return str(self.answer)
        else:
            return '-' 

def update_group(cell):
    '''Remove answer of this newly answered cell from each other cell it its group.'''
    print 'update_group ' + str(cell.get_group_number())
    for other_cell in cell_groups[cell.get_group_number()]:
        if other_cell.remove_possible_value(cell.answer):
            update(other_cell)

def update_column(cell):
    '''Remove answer of this newly answered cell from each other cell it its column.'''
    print 'update_row ' + str(cell.x)
    for other_cell in [y[cell.x] for y in cells]:
        if other_cell.remove_possible_value(cell.answer):
            update(other_cell)  
        
def update_row(cell):
    '''Remove answer of this newly answered cell from each other cell it its row.'''
    print 'update_row ' + str(cell.y)
    for other_cell in cells[cell.y]:
        if other_cell.remove_possible_value(cell.answer):
            update(other_cell)


def update(other_cell):
    '''When we remove the penultimate possible answer from a cell, it's answered.
       Relay this answer to all other cells in its group, row, and column.'''
    print_game()
    update_row(other_cell)
    update_group(other_cell)
    update_column(other_cell)

def load_initial_state(initial_state):
    '''Loads in the answers from the initial state, filling in answers as they are found.'''
    for y in range(9):
        for x in range(9):
            if initial_state[y][x] != -1:
                cells[y][x].set_answer(initial_state[y][x])
                update(cells[y][x])
                
def print_game():
    for y in range(9):
        for x in range(9):
            if cells[y][x].answer:
                print cells[y][x].answer,
            else:
                print '-',
        print ''
    print ''              

# Keep track of the 3x3 subgroups of cells
cell_groups = [[] for i in range(9)]

# Init a 2D array of cells
cells = [[Cell(x, y) for x in range(9)] for y in range(9)]


empty_gameboard = [[-1, -1, -1, -1, -1, -1, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1, -1, -1, -1]]

easy_gameboard = [[ 7, -1, -1, -1,  6,  3, -1,  5,  8],
                  [ 6, -1, -1,  2, -1, -1, -1, -1, -1],
                  [ 3,  5, -1, -1, -1, -1, -1,  2,  4],
                  [-1,  3,  4,  7,  9, -1,  5, -1, -1],
                  [-1,  6, -1, -1,  1, -1, -1,  8, -1],
                  [-1, -1,  7, -1,  2,  6,  1,  4, -1],
                  [ 4,  8, -1, -1, -1, -1, -1,  1,  5],
                  [-1, -1, -1, -1, -1,  2, -1, -1,  6],
                  [ 5,  2, -1,  6,  4, -1, -1, -1,  7]]

# Easy problems do not require speculation and can be solved just from the initial state
load_initial_state(easy_gameboard)

# For later when we have to speculate and backtrack
def get_least_constrained_value_for_row(y):
    raise NotImplementedError

def get_least_constrained_value_for_col(x):
    raise NotImplementedError

def get_least_constrained_value_for_group(x, y):
    raise NotImplementedError

def get_most_constrained_cell():
    raise NotImplementedError